<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            'title' =>  'боевик',
            'id' => 1,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'блокбастер, кассовый фильм',
            'id' => 2,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'мультфильм',
            'id' => 3,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'мелодрама',
            'id' => 4,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'комедия',
            'id' => 5,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'документальный фильм',
            'id' => 6,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'исторический фильм',
            'id' => 7,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'Драма',
            'id' => 8,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'Триллер',
            'id' => 9,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'Фантастика',
            'id' => 10,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
        DB::table('genres')->insert([
            'title' =>  'Детектив',
            'id' => 11,
            'video_id' => rand(1,11),
            'created_at' => '2020-11-12 17:15:00',
        ]);
    }
}
