<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
            'title' =>  'Титаник',
            "id" => 1
        ]);
        DB::table('videos')->insert([
            'title' =>  'Список Шиндлера',
            "id" => 2
        ]);
        DB::table('videos')->insert([
            'title' =>  'Шерлок Холмс и доктор Ватсон: Собака Баскервилей',
            "id" => 3
        ]);
        DB::table('videos')->insert([
            'title' =>  'Властелин колец: Братство кольца',
            "id" => 4
        ]);
        DB::table('videos')->insert([
            'title' =>  'Шерлок Холмс и доктор Ватсон: Охота на тигра',
            "id" => 5
        ]);
        DB::table('videos')->insert([
            'title' =>  'Властелин колец 2: Две крепости',
            "id" => 6
        ]);
        DB::table('videos')->insert([
            'title' =>  'Форрест Гамп',
            "id" => 7
        ]);
        DB::table('videos')->insert([
            'title' =>  'Шерлок Холмс и доктор Ватсон: Кровавая надпись',
            "id" => 8
        ]);
        DB::table('videos')->insert([
            'title' =>  'Побег из Шоушенка',
            "id" => 9
        ]);
        DB::table('videos')->insert([
            'title' =>  'Терминатор 2: Судный день',
            "id" => 10
        ]);
        DB::table('videos')->insert([
            'title' =>  'Властелин колец 3: Возвращение Короля',
            "id" => 11
        ]);
    }
}
