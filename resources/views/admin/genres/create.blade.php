@extends('layouts.app')
@section('content')
    <form action="{{route("admin.genres.store")}}" method="post">
        @csrf
        <label for="title">Genre</label>
        <input type="text" name="title">
        <button type="submit">Save</button>
    </form>
@endsection
