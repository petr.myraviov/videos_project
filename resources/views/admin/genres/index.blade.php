@extends('layouts.app')
@section('content')
@foreach($genres as $genre)
    <a href="{{route('admin.genres.show',['genre' => $genre])}}">{{$genre->title}}</a><br>
@endforeach
@endsection
